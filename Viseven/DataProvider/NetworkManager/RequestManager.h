//
//  RequestManager.h
//  TestTaskViseven
//
//  Created by Kostiantyn Dzetsiuk on 03.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CachedImage;

NS_ASSUME_NONNULL_BEGIN

@interface RequestManager : NSObject <NSURLSessionDataDelegate>

- (void)provideImageDictionary:(void (^)(NSArray *responseData, NSError *error))completionBlock;
- (void)download:(NSArray *)imagesArray delegate:(id <NSURLSessionDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
