//
//  RequestManager.m
//  TestTaskViseven
//
//  Created by Kostiantyn Dzetsiuk on 03.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import "RequestManager.h"
#import "CachedImage.h"

#define UNSPLASHDOMAIN @"https://api.unsplash.com/"
#define IMAGECOUNT @"20"
#define CLIENT_ID @"810ccfc599f8854496497bf2c12a733f2c01658a40c1afa500a41db7808b1bb4"

@implementation RequestManager

- (void)provideImageDictionary:(void (^)(NSArray *responseData, NSError *error))completionBlock {
    NSString *URLString = [NSString stringWithFormat:@"%@photos/random?count=%@&client_id=%@&orientation=landscape", UNSPLASHDOMAIN, IMAGECOUNT, CLIENT_ID];
    NSURL *url = [NSURL URLWithString:URLString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [[NSURLSession.sharedSession dataTaskWithRequest:request
                                   completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil && data != nil) {
            NSArray *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            [self getEntityFrom:responseDictionary
                completionBlock:^(NSMutableArray *responseArray) {
                completionBlock(responseArray, error);
            }];
        } else {
            completionBlock(nil, error);
        }
    }] resume];
}

- (void)getEntityFrom:(NSArray *)array completionBlock:(void (^)(NSMutableArray *responseData))objects {
    NSMutableArray *cachedImageArray = [NSMutableArray new];
    for (NSDictionary *dictionary in array) {
        NSURL *thumbLink = [NSURL URLWithString: dictionary[@"urls"][@"thumb"]];
        NSData *thumbImage = [NSData dataWithContentsOfURL:thumbLink];
        NSString *description = dictionary[@"description"];
        NSString *name = dictionary[@"user"][@"username"];
        NSString *fullImageLink = dictionary[@"urls"][@"full"];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:description, @"info", name, @"userName", fullImageLink, @"fullSizeImageUrl", thumbImage, @"thumbnailImage", nil];
        [cachedImageArray addObject:dictionary];
    }
    objects(cachedImageArray);
}

- (void)download:(NSArray *)imagesArray delegate:(id <NSURLSessionDelegate>)delegate {
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfig setHTTPMaximumConnectionsPerHost:3];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:delegate delegateQueue:nil];
    for (CachedImage *object in imagesArray) {
        NSURL *url = [NSURL URLWithString:object.fullSizeImageUrl];
        NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:url];
        [downloadTask resume];
    }
}


@end

