//
//  DataProvider.m
//  TestTaskViseven
//
//  Created by Kostiantyn Dzetsiuk on 03.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import "DataProvider.h"
#import "RequestManager.h"
#import "FetchedResultsController.h"

@interface DataProvider()

@property(strong, nonatomic) RequestManager *requestManager;
@property(strong, nonatomic) FetchedResultsController *fetchedResultsController;

@end

@implementation DataProvider

- (NSArray *)getDataArray {
    return self.fetchedResultsController.resultsController.fetchedObjects;
}

- (void)makeRequestAndSaveData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.requestManager provideImageDictionary:^(NSArray * _Nonnull responseData, NSError * _Nonnull error) {
            if (error != nil || responseData == nil) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dataUpdate"
                                                                    object:self.fetchedResultsController.resultsController.fetchedObjects];
            } else {
                [self importEntities:responseData];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dataUpdate"
                                                                    object:self.fetchedResultsController.resultsController.fetchedObjects];
            }
        }];
    });
}


- (void)importEntities:(NSArray *)entities {
    [self.fetchedResultsController import:entities];
}

- (void)downloadImages:(NSArray *)imagesArray {
    NSMutableArray *downloadArray = [NSMutableArray new];
    for (NSNumber *number in imagesArray) {
        NSInteger index = [number integerValue];
        [downloadArray addObject:[self.fetchedResultsController.resultsController fetchedObjects][index]];
    }

    [self.requestManager download:downloadArray delegate:self];
}

- (void)URLSession:(nonnull NSURLSession *)session downloadTask:(nonnull NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(nonnull NSURL *)location {
    NSString *url = downloadTask.originalRequest.URL.absoluteString;
    [self.fetchedResultsController changeData:[NSData dataWithContentsOfURL: location] forEntityWithUrl:url];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
}

- (RequestManager *)requestManager {
    _requestManager = [[RequestManager alloc] init];
    return _requestManager;
}

- (FetchedResultsController *)fetchedResultsController {
    _fetchedResultsController = [[FetchedResultsController alloc] init];
    return _fetchedResultsController;
}

@end
