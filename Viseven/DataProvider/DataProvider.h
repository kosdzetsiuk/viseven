//
//  DataProvider.h
//  TestTaskViseven
//
//  Created by Kostiantyn Dzetsiuk on 03.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CachedImage;
NS_ASSUME_NONNULL_BEGIN

@interface DataProvider : NSObject <NSURLSessionDataDelegate>

- (NSArray<CachedImage *> *)getDataArray;
- (void)makeRequestAndSaveData;
- (void)downloadImages:(NSArray *)imagesArray;

@end

NS_ASSUME_NONNULL_END
