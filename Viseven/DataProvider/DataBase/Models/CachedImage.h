//
//  CachedImage.h
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 06.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CachedImage : NSManagedObject

@property (nullable, nonatomic, copy) NSString *info;
@property (nullable, nonatomic, retain) NSData *thumbnailImage;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, retain) NSData *fullSizeImage;
@property (nullable, nonatomic, retain) NSString *fullSizeImageUrl;
@property (nonatomic, assign) BOOL isDownloaded;


+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext*)context;
- (void)loadFromDictionary:(NSDictionary *)dictionary;
+ (NSFetchRequest<CachedImage *> *)fetchRequest;

@end

NS_ASSUME_NONNULL_END
