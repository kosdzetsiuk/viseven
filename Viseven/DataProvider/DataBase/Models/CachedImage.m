//
//  CachedImage.m
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 06.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import "CachedImage.h"

@implementation CachedImage

@dynamic info;
@dynamic thumbnailImage;
@dynamic userName;
@dynamic fullSizeImage;
@dynamic fullSizeImageUrl;
@dynamic isDownloaded;

- (void)loadFromDictionary:(NSDictionary *)dictionary {
    self.fullSizeImageUrl = dictionary[@"fullSizeImageUrl"];
    self.userName = dictionary[@"userName"];
    self.thumbnailImage = dictionary[@"thumbnailImage"];
    self.isDownloaded = NO;
    if (![dictionary[@"info"] isKindOfClass:[NSNull class]]) {
        self.info = dictionary[@"info"];
    } else {
        // description doesn`t found so create custom for example: "username + description"
        self.info = [NSString stringWithFormat:@"%@ + custom description", self.userName];
    }
}

+ (NSFetchRequest<CachedImage *> *)fetchRequest {
    return [NSFetchRequest fetchRequestWithEntityName:@"CachedImage"];
}

+ (id)entityName {
    return NSStringFromClass(self);
}

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext*)context {
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityName] inManagedObjectContext:context];
}

@end
