//
//  FetchedResultsController.m
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 05.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import "FetchedResultsController.h"
#import "CachedImage.h"

@implementation FetchedResultsController

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Viseven"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
        _persistentContainer.viewContext.automaticallyMergesChangesFromParent = YES;
        _persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        _persistentContainer.viewContext.shouldDeleteInaccessibleFaults = YES;
    }
    return _persistentContainer;
}

- (void)import:(NSArray *)entitiesArray {
    NSManagedObjectContext *taskContext = [self.persistentContainer newBackgroundContext];
    taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    taskContext.undoManager = nil;
    [taskContext performBlockAndWait:^{
        for(NSDictionary *dictionary in entitiesArray) {
            NSManagedObject *cachedImage = [CachedImage insertNewObjectIntoContext:taskContext];
            [((CachedImage *)cachedImage) loadFromDictionary:dictionary];
        }
        if (taskContext.hasChanges) {
            NSError *error = nil;
            @try {
                [self deleteCachingObjects];
                [taskContext save:&error];
                NSLog(@"Saved");
            }
            @catch (NSException *exception) {
               NSLog(@"%@", exception.reason);
            }
            [taskContext reset];
        }
    }];
}

- (CachedImage *)getEntityForUrl:(NSString *)url {
    NSArray *fetchedObjects = [self.resultsController fetchedObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fullSizeImageUrl == %@", url];
    CachedImage *entity = [fetchedObjects filteredArrayUsingPredicate:predicate].firstObject;
    return entity;
}

- (void)changeData:(NSData *)data forEntityWithUrl:(NSString *)url  {
    CachedImage *entity = [self getEntityForUrl:url];
    entity.fullSizeImage = data;
    entity.isDownloaded = YES;
    [self saveContext];
}

- (void)deleteCachingObjects {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CachedImage"];
    NSManagedObjectContext *taskContext = [self.persistentContainer newBackgroundContext];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    NSError *deleteError = nil;
    [taskContext executeRequest:delete error:&deleteError];
    NSLog(@"Deleted");
}

- (NSFetchedResultsController *)resultsController {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CachedImage"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"userName" ascending:YES]];
    NSFetchedResultsController *controller = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                 managedObjectContext:self.persistentContainer.viewContext
                                                                                   sectionNameKeyPath:nil
                                                                                            cacheName:nil];
    controller.delegate = self.fetchedResultsControllerDelegate;
    NSError *error = nil;
    @try {
        [controller performFetch:&error];
    }
    @catch (NSException *exception) {
       NSLog(@"%@", exception.reason);
    }
    return controller;
}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
