//
//  FetchedResultsController.h
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 05.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <CoreData/CoreData.h>

@class CachedImage;
NS_ASSUME_NONNULL_BEGIN

@interface FetchedResultsController : NSObject <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic, readonly) NSPersistentContainer *persistentContainer;
@property (weak, nonatomic) id<NSFetchedResultsControllerDelegate> fetchedResultsControllerDelegate;
@property (nonatomic, strong) NSFetchedResultsController* resultsController;

- (void)import:(NSArray *)entitiesArray;
- (void)changeData:(NSData *)data forEntityWithUrl:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
