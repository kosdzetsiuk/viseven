//
//  SceneDelegate.h
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 05.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

