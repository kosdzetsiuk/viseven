//
//  CustomCollectionViewCell.m
//  TestTaskViseven
//
//  Created by Kostiantyn Dzetsiuk on 03.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import "CustomCollectionViewCell.h"

@implementation CustomCollectionViewCell

- (void)configureWithEntity:(CachedImage *)entity {
    self.cellImageView.image = [UIImage imageWithData:entity.thumbnailImage];
    self.descriptionLabel.text = entity.info;
}

- (void)toggleSelected {
    self.layer.masksToBounds = YES;
    self.layer.borderWidth=2.0f;
    if (self.selected) {
        self.layer.borderColor = [UIColor blackColor].CGColor;
    } else {
        return;
    }
}

@end
