//
//  CustomCollectionViewCell.h
//  TestTaskViseven
//
//  Created by Kostiantyn Dzetsiuk on 03.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CachedImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressIndicator;

- (void)configureWithEntity:(CachedImage *)entity;
- (void)toggleSelected;

@end

NS_ASSUME_NONNULL_END
