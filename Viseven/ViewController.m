//
//  ViewController.m
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 05.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import "ViewController.h"
#import "CustomCollectionViewCell.h"
#import "CachedImage.h"
#import "DataProvider.h"
#import "FetchedResultsController.h"
#import "ImageViewController.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *indexesForDownload;
@property (strong, nonatomic) FetchedResultsController *dataController;
@property (strong, nonatomic) DataProvider *dataProvider;

@end

@implementation ViewController
{
    UIActivityIndicatorView *spinner;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self activityIndicator];
    [spinner startAnimating];
    self.collectionView.allowsMultipleSelection = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateData:)
                                                 name:@"dataUpdate"
                                               object:nil];
    self.indexesForDownload = [NSMutableArray new];
}

- (void)updateData:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->spinner stopAnimating];
        self.collectionView.hidden = NO;
        [self.collectionView reloadData];
    });
}

- (IBAction)download:(id)sender {
    self.downloadButton.hidden = YES;
    [self.dataProvider downloadImages:self.indexesForDownload];
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [self.dataController.resultsController fetchedObjects].count;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView
                                   cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"customCell";
    CustomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: cellIdentifier forIndexPath: indexPath];
    if (cell == nil) {
        return [CustomCollectionViewCell new];
    }
    CachedImage *entity = [self.dataController.resultsController fetchedObjects][indexPath.row];
    [cell configureWithEntity:entity];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CustomCollectionViewCell *cell = (CustomCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell toggleSelected];
    CachedImage *entity = [self.dataController.resultsController fetchedObjects][indexPath.row];
    if (entity.isDownloaded && entity.fullSizeImage != nil) {
        ImageViewController *imageView = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondVC"];;
        imageView.image = [UIImage imageWithData:entity.fullSizeImage];
        [self.navigationController pushViewController:imageView animated:YES];
    } else if ([self.indexesForDownload containsObject:[NSNumber numberWithInteger: indexPath.row]]) {
        [self.indexesForDownload removeObject:[NSNumber numberWithInteger: indexPath.row]];
    } else {
        self.downloadButton.hidden = NO;
        [self.indexesForDownload addObject:[NSNumber numberWithInteger: indexPath.row]];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat padding = 50.0;
    CGFloat collectionViewSize = collectionView.frame.size.width - padding;
    return CGSizeMake(collectionViewSize/2, collectionViewSize/1.5);
}

- (void)activityIndicator {
    self.collectionView.hidden = YES;
    spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleLarge;
    spinner. center = self.view.center;
    spinner.color = [UIColor blackColor];
    [self.view addSubview:spinner];
    
}

- (FetchedResultsController *)dataController {
    _dataController = [FetchedResultsController new];
    return _dataController;
}

- (DataProvider *)dataProvider {
    return [DataProvider new];
}


@end
