//
//  ViewController.h
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 05.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *downloadButton;

@end

