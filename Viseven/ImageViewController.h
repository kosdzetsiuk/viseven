//
//  ImageViewController.h
//  Viseven
//
//  Created by Kostiantyn Dzetsiuk on 06.11.2019.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) UIImage *image;
@end

NS_ASSUME_NONNULL_END
